Create an .env file with:

DB_USER=registry
DB_PASSWORD=........
DB_PASSWORD_PG=..........
DB_NAME=registry
REDIS_PASSWORD=............

# Prepare a server certificate for the CN of your DNS name of the registry engine

# Bootstrap the database

docker-compose up db

# the statement below is automatically created by the initscripts on the DB image
# Use the administrative user 'postgres' and alter schema of the 'registry' DB instance
[ use the postgres password entry in .env file DB_PASSWORD_PG ]
CREATE EXTENSION IF NOT EXISTS pg_trgm;


# Start the configuration instance of quay
docker-compose run --rm -p 443:8443 engine config my-secret-password

# Start the redis instance
docker-compose up -d cache

# Access the web ui of the config instance
use user name quayconfig and the password provided in the command line
https://<docker host ip>/
Login with quayadmin and the password provided in the command line above (my-secret-password)

# If you use a redis instance, populate is service name and password
cache
_password of redis cdache_

# copy the downloaded config in place, start the quay engine instance
# Stop the quay config instance
# Start the quay engine instance




#!/bin/bash
set -e

echo "CREATE EXTENSION IF NOT EXISTS pg_trgm;" | psql -e -U "$POSTGRES_USER" -d "$POSTGRES_DB"

